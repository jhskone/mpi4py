# Distributed-memory Parallel Programming with MPI in Python Workshop (mpi4py)

Materials for the Distributed-memory Parallel Programming with MPI in Python Workshop (Winter 2019)

## Objectives
Learning Outcomes for attendees of this workshop:

* Able to understand the differences between shared memory and distributed memory processing and known when each is appropriate
* Understand the MPI concepts of point-to-point communication, collective communication, and one-sided communication
* Be able to write a simple python program using MPI
* Know how to setup your python environment to utilize mpi4py package and be able to execute such jobs on the cluster

## Outline

* [Parallel Computing Overview and Background on the Message Passing Interface](pdfs/overview.pdf)

* [Lab1:](notebooks/labs/lab1/lab1.ipynb) Simple working MPI example -- Hello world 

* [Lab2:](notebooks/labs/lab2/lab2.ipynb) Point-to-Point Communications (Send | Recv)

* [Lab3:] Blocking vs Non-Blocking

* [Lab4:] Collective Communications
 
* [Lab5:] Collective Communications 2

* [Lab6:] Real Example of MPI Parallel Code

* Additional Exercises -- See the exercises folder

## Running the Notebooks for this Workshop

We will use Midway compute nodes to run the lab notebooks and examples. In order to do
so you should log on to midway. If you do not have an account or this is your first time
accessing midway, see the [Connecting to Midway Document ](pdfs/connecting_midway.pdf). 

Once logged on to midway, if you do not want to run the notebooks from your home directory, 
change directory to a location of your choice. It is generally recommended that  
compute jobs are run from your SCRATCH directory (/scratch/midway2/$USER).  

Then clone this repository:

```bash
git clone https://gitlab.com/jhskone/mpi4py.git
```

Then change directory into the repository directory and run the `launch.nb` 
script to start a jupyter lab session on a compute node.

```bash
cd mpi4py 
./launch-nb.sh
```

The launch.nb script will prompt you with instructions for
connecting to the jupyter lab session created for you on the compute nodes.If you are on
the UofC campus network, you should be able to directly copy the first URL link printed
into your local (i.e. laptop) web browser and connect to the running jupyter lab session.
Note that if one is not on the campus network, but would prefer to access the jupyter lab
session in this manner, one needs to first connect to the campus VPN first, before running
the `launch.nb` script. If not on the campus network, you will have two steps to follow
in order to successfully connect to the running jupyter lab session on the midway compute
node. Follow the two-step instructions printed by the launch.nb script, where you must
first tunnel with the remote server and then launch your jupyter lab session on your
local machine using the `localhost` address as displayed in the output from launch-nb.sh

### Jupyter Lab and Notebooks

For those not familiar with the Jupyter Lab IDE or Jupyter Notebooks, there is a separate [Intro
to Jupyter notebook](notebooks/jupyter_intro.ipynb) that covers the rudimentary aspects of working with Jupyter Lab and Jupyter Notebooks
